use std::collections::*;

pub trait Collection {
    type Item;

    fn len(&self) -> usize;
    fn is_empty(&self) -> bool {
        self.len() == 0
    }
    fn clear(&mut self);
}

macro_rules! impl_collection {
    ($t:ident) => {
        impl<T> Collection for $t<T> {
            type Item = T;

            #[inline]
            fn len(&self) -> usize {
                self.len()
            }

            #[inline]
            fn is_empty(&self) -> bool {
                self.is_empty()
            }

            #[inline]
            fn clear(&mut self) {
                self.clear()
            }
        }
    };
    ($t:ident, $($rest:ident), +) => {
        impl_collection!($t);
        impl_collection!($($rest),+);
    };
}

macro_rules! impl_collection_map {
    ($t:ident) => {
        impl<K, V> Collection for $t<K, V> {
            type Item = V;

            #[inline]
            fn len(&self) -> usize {
                self.len()
            }

            #[inline]
            fn is_empty(&self) -> bool {
                self.is_empty()
            }

            #[inline]
            fn clear(&mut self) {
                self.clear()
            }
        }
    };
    ($t:ident, $($rest:ident), +) => {
        impl_collection_map!($t);
        impl_collection_map!($($rest),+);
    };
}

impl_collection!(Vec, VecDeque, LinkedList, HashSet, BTreeSet, BinaryHeap);
impl_collection_map!(HashMap, BTreeMap);
